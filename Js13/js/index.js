" use strict ";

const slides = document.querySelectorAll(".image-to-show");
const stopButton = document.getElementById("stopButton");
const resumeButton = document.getElementById("resumeButton");

let currentSlide = 0;
let slideshowInterval;

function showSlide(slideIndex) {
  slides.forEach((slide) => slide.classList.remove("active"));
  slides[slideIndex].classList.add("active");
}

function startSlideshow() {
  slideshowInterval = setInterval(() => {
    currentSlide = (currentSlide + 1) % slides.length;
    showSlide(currentSlide);
  }, 3000);
}

function stopSlideshow() {
  clearInterval(slideshowInterval);
}

stopButton.addEventListener("click", () => {
  stopSlideshow();
});

resumeButton.addEventListener("click", () => {
  showSlide(currentSlide);
  startSlideshow();
});

showSlide(currentSlide);
startSlideshow();

// setTimeout() - виконує якусь операцію с затримкою один раз  setInterval()`- може виконувати декілька разів операцію з інтервалом який ми визначимо
// Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому? Я думаю що спрацюе , бо нульова затримка теж значення
// Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен? clear інтервал допомагає завершити нашу дію наших методів
