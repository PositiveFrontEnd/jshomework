" use strict ";
const arr = [
  "hello",
  "world",
  23,
  "23",
  null,
  true,
  undefined,
  { userName: "Yevhen" },
];
function filterBy(array, dataType) {
  return array.filter((item) => typeof item !== dataType);
}

const allTypes = ["string", "number", "boolean", "undefined", "object"];
allTypes.forEach((type) => {
  console.log(type, filterBy(arr, type));
});
