" use strict ";

function studentInfo() {
  const student = {
    name: "",
    lastName: "",
  };

  let userName = prompt("Введіть ім'я ");
  while (!isNaN(parseInt(userName)) || userName === null || userName === "") {
    userName = prompt("Введіть ім'я ");
  }
  student.name = userName;

  let userLastName = prompt("Введіть ваше прізвище");
  while (
    !isNaN(parseInt(userLastName)) ||
    userLastName === null ||
    userLastName === ""
  ) {
    userLastName = prompt("Введіть ваше прізвище");
  }
  student.lastName = userLastName;

  let badGradesCount = 0;
  if (student.tabel === undefined) student.tabel = {};
  while (true) {
    let nameItem = prompt("Введіть назву предмету");
    if (nameItem === null) break;
    while (!isNaN(parseInt(nameItem)) || nameItem === "") {
      nameItem = prompt("Введіть назву предмету");
      if (nameItem === null) break;
    }
    let value = +prompt("Введіть оцінку");
    while (
      isNaN(parseInt(value)) ||
      value === null ||
      value === "" ||
      value < 0 ||
      value > 12
    ) {
      value = +prompt("Введіть оцінку");
    }
    student.tabel[nameItem] = value;
    if (value < 4) {
      badGradesCount++;
    }
  }

  let sum = 0;
  for (const key in student.tabel) {
    sum += student.tabel[key];
  }

  let countUser = Object.keys(student.tabel).length;
  if (sum / countUser >= 7) {
    alert("Студенту призначено стипендію");
  }
  badGradesCount > 0
    ? alert(`Студент має ${badGradesCount}  оцінок нижче 4 `)
    : alert("Вас переведено на наступний рік");

  return student;
}
const student = studentInfo();
console.log(student);
