" use strict ";

const object = {
  first: 1,
  second: 2,
  obj1: {
    first: 1,
    second: 2,
  },
  fourth: 4,
  arr1: [1, 2, 3, 4, 5],
  obj2: {
    obj3: { arr2: [6, 7, 8, 9, 10] },
  },
};
console.log(object);
let newObject = {};
const newArray = Object.entries(object).forEach(([key, value]) => {
  newObject[key] = value;
});
console.log(newObject);
