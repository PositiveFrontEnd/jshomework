"use strict";

function createNewUser() {
  let newUser = {};

  let firstName = prompt("Введіть ваше ім'я ");
  while (!isNaN(firstName) || firstName === null || firstName === "") {
    firstName = prompt("Введіть ваше ім'я ");
  }
  let lastName = prompt(" Введіть ваше прізвише");
  while (!isNaN(lastName) || lastName === null || lastName === "") {
    lastName = prompt(" Введіть ваше прізвише");
  }

  newUser.getLogin = function () {
    return (
      this.firstName.slice(0, 1).toLocaleLowerCase() +
      this.lastName.toLocaleLowerCase()
    );
  };
  Object.defineProperty(newUser, "firstName", {
    value: firstName,
    writable: false,
    configurable: true,
  });

  Object.defineProperty(newUser, "lastName", {
    value: lastName,
    writable: false,
    configurable: true,
  });

  newUser.setFirstName = function () {
    return Object.defineProperty(newUser, "firstName", {
      writable: true,
    });
  };

  newUser.setLastName = function () {
    return Object.defineProperty(newUser, "lastName", {
      writable: true,
    });
  };

  return newUser;
}

let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.setFirstName());
console.log(newUser.setLastName());
newUser.firstName = "Bob";
newUser.lastName = "Marley";
console.log(newUser);

//Опишіть своїми словами, що таке метод об’єкту .  Метод об’єкту це якась дія яку ми можеме використовувати з об'єктом .
//Який тип даних може мати значення властивості об’єкта? Будь який )
//Об’єкт це посилальний тип даних. Що означає це поняття? Це означае що об'єкт не зберігає в собі інформацію , він мае ссилку до цієї інформації.
