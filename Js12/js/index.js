" use strict ";
const button = document.querySelectorAll(".btn");
document.addEventListener("keydown", (event) => {
  button.forEach((item) => {
    let b = item.innerText;
    let code = event.code;
    item.classList.remove("active");
    if (code === b || code[3] === b) {
      item.classList.add("active");
    }
  });
});
//  Чому не краше не використовувати взаемодію з клавіатурою у формі , я думаю що в формі ми використовуемо майже всі клавіші і краше не варто на них щось ще вішати якісь дії!
