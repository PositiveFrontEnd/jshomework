" use strict ";
const buttonCheng = document.querySelector(".button_cheng");
const section = document.querySelector(".wrap_clients_projects");
buttonCheng.addEventListener("click", (event) => {
  event.preventDefault();
  if (localStorage.getItem("theme") === "dark") {
    localStorage.removeItem("theme");
  } else {
    localStorage.setItem("theme", "dark");
  }
  addDarkClassToHtml();
});
function addDarkClassToHtml() {
  try {
    if (localStorage.getItem("theme") === "dark") {
      document.querySelector("body").classList.add("dark");
      section.style.background = "white";
      buttonCheng.style.background = "#171717";
      buttonCheng.style.color = "white";
    } else {
      document.querySelector("body").classList.remove("dark");
      section.style.background = "#0e0e0e";
      buttonCheng.style.background = "white";
      buttonCheng.style.color = "#171717";
    }
  } catch (err) {}
}
addDarkClassToHtml();
