" use strict ";
const elementsByTag = document.getElementsByTagName("p");
console.log(elementsByTag);
for (let node of elementsByTag) {
  node.style.backgroundColor = "#ff0000";
}
const getElementById = document.getElementById("optionsList");
const parentElement = getElementById.parentElement;
const children = parentElement.childNodes;
console.log(getElementById);
console.log(parentElement);
console.log(children);
for (const child of children) {
  // if (child.nodeType === Node.ELEMENT_NODE) {
  console.log(`Назва: ${child.nodeName}, Тип: ${child.nodeType}`);
}
// }

const querySelector = document.querySelector("#testParagraph");
querySelector.textContent = "This is a paragraph";
console.log(querySelector);

const getElementsBy = document.querySelector(".main-header");

const childrens = getElementsBy.children;
for (const iterator of childrens) {
  iterator.classList.add("nav-item");
}

console.log(getElementsBy);

const getElementSection = document.querySelectorAll(".section-title");

for (const element of getElementSection) {
  element.classList.remove("section-title");
  console.log(element);
}
console.log(getElementSection);

// Це модель яка дає можливість працювати с документом
//  HTML-елементів ;innerHTML - це внутреннее содержимое узла-элемента  ;innerText- це можливість працювати з техтом та стилями
//  getElementById звертаємось до айді єлемента , getElementsByTagName звертаємось за тегом до єлемента , getElementsByClassName звертаємось за класом єлемента
// getElementsByName звертаємось по імені
// querySelector звертається до першого селектора який ми напишемо в дужках будь який
// querySelectorAll звертається до всіх селекторів які ми напишемо в дужках
// кажуть що найчастіше використовують querySelector , querySelectorAll .
