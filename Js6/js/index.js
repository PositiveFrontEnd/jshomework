"use strict";

function createNewUser() {
  let newUser = {};

  let firstName = prompt("Введіть ваше ім'я ");
  while (!isNaN(firstName) || firstName === null || firstName === "") {
    firstName = prompt("Введіть ваше ім'я ");
  }
  let lastName = prompt(" Введіть ваше прізвише");
  while (!isNaN(lastName) || lastName === null || lastName === "") {
    lastName = prompt(" Введіть ваше прізвише");
  }

  newUser.getLogin = function () {
    return (
      this.firstName.slice(0, 1).toLocaleLowerCase() +
      this.lastName.toLocaleLowerCase()
    );
  };
  newUser.getPassword = function () {
    return (
      this.firstName.slice(0, 1).toUpperCase() +
      this.lastName.toLocaleLowerCase()
    );
  };
  Object.defineProperty(newUser, "firstName", {
    value: firstName,
    writable: false,
    configurable: true,
  });

  Object.defineProperty(newUser, "lastName", {
    value: lastName,
    writable: false,
    configurable: true,
  });

  newUser.setFirstName = function () {
    return Object.defineProperty(newUser, "firstName", {
      writable: true,
    });
  };

  newUser.setLastName = function () {
    return Object.defineProperty(newUser, "lastName", {
      writable: true,
    });
  };

  const dateYear = new Date().getFullYear();
  const dateMonth = new Date().getMonth() + 1;
  const dateDate = new Date().getDate();

  let bersdeyDay = prompt("Введите дату в формате дд.мм.гггг");
  let number = bersdeyDay.split(".");

  let dateFormat = `${number[2]}/${number[1]}/${number[0]}`;

  while (
    isNaN(Date.parse(dateFormat)) ||
    number.length !== 3 ||
    bersdeyDay === null ||
    bersdeyDay === ""
  ) {
    bersdeyDay = prompt("Введите дату в формате дд.мм.гггг");
    number = bersdeyDay.split(".");
  }

  newUser.bersdey = new Date(dateFormat);

  newUser.getAge = function () {
    if (
      dateYear < Number(number[2]) ||
      (dateYear === Number(number[2]) &&
        (dateMonth < Number(number[1]) ||
          (dateMonth === Number(number[1]) && dateDate < Number(number[0]))))
    ) {
      return alert("Не правильна дата");
    } else if (dateYear === Number(number[2])) {
      return `Наразі вам ${dateMonth - Number(number[1])} місяців ${
        dateDate - Number(number[0])
      } днів`;
    } else if (dateMonth < Number(number[1])) {
      return dateYear - Number(number[2]) - 1;
    } else {
      return dateYear - Number(number[2]);
    }
  };

  return newUser;
}

let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.getPassword());
console.log(newUser.setFirstName());
console.log(newUser.setLastName());
console.log(newUser.getAge());
newUser.firstName = "Bob";
newUser.lastName = "Marley";
console.log(newUser);

// Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування > Єкранування це спосиб запису рядкив , він потрибен щоб можна було маніпулювати тим чи іншим записом

// Які засоби оголошення функцій ви знаєте? Е два способи це коли ми прописуємо function (){} і другий через змінну let result = function () {}

// Що таке hoisting, як він працює для змінних та функцій? Хостінг це процес розміщення данних , чесна скажу навіть не маю поняття , але я почитаю
// ("use strict");

// function createNewUser() {
//   let newUser = {};

//   let firstName = prompt("Введіть ваше ім'я");
//   while (!isNaN(firstName) || firstName === null || firstName === "") {
//     firstName = prompt("Введіть ваше ім'я");
//   }
//   let lastName = prompt("Введіть ваше прізвище");
//   while (!isNaN(lastName) || lastName === null || lastName === "") {
//     lastName = prompt("Введіть ваше прізвище");
//   }

//   newUser.getLogin = function () {
//     return (
//       this.firstName.slice(0, 1).toLocaleLowerCase() +
//       this.lastName.toLocaleLowerCase()
//     );
//   };
//   newUser.getPassword = function () {
//     return (
//       this.firstName.slice(0, 1).toUpperCase() +
//       this.lastName.toLocaleLowerCase()
//     );
//   };
//   Object.defineProperty(newUser, "firstName", {
//     value: firstName,
//     writable: false,
//     configurable: true,
//   });

//   Object.defineProperty(newUser, "lastName", {
//     value: lastName,
//     writable: false,
//     configurable: true,
//   });

//   newUser.setFirstName = function () {
//     return Object.defineProperty(newUser, "firstName", {
//       writable: true,
//     });
//   };

//   newUser.setLastName = function () {
//     return Object.defineProperty(newUser, "lastName", {
//       writable: true,
//     });
//   };

//   const currentDate = new Date();
//   const dateYear = currentDate.getFullYear();
//   const dateMonth = currentDate.getMonth() + 1;
//   const dateDate = currentDate.getDate();

//   let birthday = prompt("Введіть дату народження в форматі дд.мм.рррр");
//   let number = birthday.split(".");
//   let day = parseInt(number[0], 10);
//   let month = parseInt(number[1], 10);
//   let year = parseInt(number[2], 10);

//   while (
//     isNaN(day) ||
//     isNaN(month) ||
//     isNaN(year) ||
//     day < 1 ||
//     day > 31 ||
//     month < 1 ||
//     month > 12 ||
//     year < 1900 ||
//     year > dateYear ||(year === dateYear &&(month > dateMonth || (month === dateMonth && day > dateDate))) ||birthday === null ||birthday === ""
//   ) {
//     birthday = prompt("Введіть дату народження в форматі дд.мм.рррр");
//     number = birthday.split(".");
//     day = parseInt(number[0], 10);
//     month = parseInt(number[1], 10);
//     year = parseInt(number[2], 10);
//   }

//   newUser.birthday = new Date(year, month - 1, day);

//   newUser.getAge = function () {
//     const birthdayYear = this.birthday.getFullYear();
//     const birthdayMonth = this.birthday.getMonth() + 1;
//     const birthdayDate = this.birthday.getDate();

//     if (
//       dateYear < birthdayYear ||
//       (dateYear === birthdayYear &&
//         (dateMonth < birthdayMonth ||
//           (dateMonth === birthdayMonth && dateDate < birthdayDate)))
//     ) {
//       return "Неправильна дата";
//     } else if (dateYear === birthdayYear) {
//       const months = dateMonth - birthdayMonth;
//       const days = dateDate - birthdayDate;
//       return `Наразі вам ${months} місяців ${days} днів`;
//     } else if (dateMonth < birthdayMonth) {
//       return dateYear - birthdayYear - 1;
//     } else {
//       return dateYear - birthdayYear;
//     }
//   };

//   return newUser;
// }

// let newUser = createNewUser();
// console.log(newUser);
// console.log(newUser.getLogin());
// console.log(newUser.getPassword());
// console.log(newUser.setFirstName());
// console.log(newUser.setLastName());
// console.log(newUser.getAge());
// newUser.firstName = "Bob";
// newUser.lastName = "Marley";
// console.log(newUser);
