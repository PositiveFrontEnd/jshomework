" use strict ";

const arr = ["hello", "world", "Kiev", [1, 2, 3], "Kharkiv", "Odessa", "Lviv"];
function takeArr(array, parent = document.body) {
  let list = document.createElement("ul");
  array.map((value) => {
    if (!Array.isArray(value)) {
      let li = document.createElement("li");
      li.textContent = value;
      list.appendChild(li);
    } else {
      let list2 = document.createElement("ul");
      let array2 = value;
      function takeArr2() {
        array2.forEach((value) => {
          let li2 = document.createElement("li");
          li2.textContent = value;
          list2.appendChild(li2);
        });
        list.append(list2);
      }
      takeArr2();
    }
  });
  parent.prepend(list);
}
takeArr(arr);
