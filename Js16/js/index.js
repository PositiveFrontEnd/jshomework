" use strict ";
let n = parseInt(prompt("Введіть число"));

function fibonacci(n) {
  if (n === 1) {
    return 1;
  }
  if (n === -1) {
    return -1;
  }
  if (n === 0) {
    return 0;
  }
  let f0 = 0;
  let f1 = 1;
  let f2;
  if (n > 0) {
    for (let i = 0; i <= n; i++) {
      f2 = f0 + f1;
      f0 = f1;
      f1 = f2;
    }
  } else {
    for (let i = 0; i >= n; i--) {
      f2 = f0 - f1;
      f0 = f1;
      f1 = f2;
    }
  }

  return f1;
}
console.log(fibonacci(n));

// як можна пофіксити те що негативні числа рахуються тільки парні , не парні рахуються без -
